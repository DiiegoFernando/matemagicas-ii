<?php

namespace App\Controller;

use App\Entity\Theme;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PrimaryController extends Controller
{
    /**
     * @Route("/MatematicaAritmetica/", name="MatematicaAritmetica")
     */
    public function MatematicaAritmetica(){
        $MATEMATICAARITMETICA = 1;
        $response = $this->forward('App\Controller\StandardController::SearchSection', array('section'=>$MATEMATICAARITMETICA));
        return $response;
    }

    /**
     * @Route("/Geometria/", name="Geometria")
     */
    public  function geometria(){
        $GEOMETRIA = 2;
        $response = $this->forward('App\Controller\StandardController::SearchSection', array(
           'section'=>$GEOMETRIA
        ));
        return $response;
    }

    /**
     * @Route("/figurageometrica/", name="figurageometrica")
     */
    public function figurageometrica(){
        return $this->render('Geometria/figurageometrica.html.twig');
    }
}
